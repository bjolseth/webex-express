# Webex-express: Bot framework for Webex inspired by express

There are multiple bot frameworks for Webex already. Use this one if you want:

* Web socket instead of web hooks (means your bot server doesn't need to be publicly available)
* A express-like router and middleware based style, so its easy to create and share middleware

It's based on the official webex sdk, and all of its API is also available from app.webex() 

## Usage

Example of a bot that responds to hello and help:

```
const app = require('webex-express');

const token = 'your-bot-token-here';
app.message(/help/, (req, res) => res.answer('Try saying hello'));
app.message(/hello/, (req, res) => res.answer('Hello yourself, ' + req.message.personEmail));

app.listen(token);
```

Example of a custom middleware (for logging):

```
const app = require('webex-express');

app.using((req, res) => {
  console.log('INCOMING:', req.text);

  // return true to stop anyone else from processing request. similar to `next` in express
  return false;
});

const token = 'your-bot-token-here';
app.listen(token);
```

