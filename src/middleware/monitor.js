/**
 * Uptime middle ware to check if bot is responding.
 * Works by sending a heat beat periodically from a different bot that
 * user must specify.
 *
 * Monitors incoming messages, if no message within a certain time,
 * notifies that bot is down. User may then reconnect etc.
 * Optionally sends webex message to user when service goes down/up again.
 *
 * Usage: callback, notifyEmail, botEmail
 * const options = {
 *  heartBeatSeconds: int,   // how often to send heart beats
 *  callback: function,           // called when outage is detected
 *  botEmail: string,             // email to bot we are monitoring
 *  token: string,                // email to bot that is monitoring uptime (must be another one)
 * };
 * const monitor = require('./express-bot/middleware/uptime-monitor');
 * app.using(monitor(options));
 */

const Webex = require('webex');

let options;
let lastMsgTime = new Date().getTime();
let isUp = true;
let webex;

const pollIntervalSeconds = 60;

function onMsg() {
  const { notifyEmail, botEmail } = options;
  if (!isUp && botEmail) {
    console.log('Bot is back up');
    const text = `Bot is responding again: ${botEmail}`;
    webex.messages.create({ toPersonEmail: notifyEmail, text });
  }
  lastMsgTime = new Date().getTime();
  isUp = true;
  return false; // dont break chain
}

function sendHeartBeat() {
  const { token, botEmail } = options;
  // console.log('Send heart beat to', botEmail);
  webex.messages.create({ toPersonEmail: botEmail, text: 'Ping' });
}

function check() {
  const now = new Date().getTime();
  const timeSinceLast = (now - lastMsgTime) / 1000;
  const { heartBeatSeconds, callback, notifyEmail, botEmail } = options;
  console.log('Checking, seconds since last', timeSinceLast);

  const margin = 10; // allow some time for the message to go through
  const tooOld = timeSinceLast > (heartBeatSeconds + 10);
  if (tooOld && isUp) {
    isUp = false;
    if (callback) {
      callback();
    }
    if (notifyEmail) {
      const text = `Bot is not responding: ${botEmail}`;
      webex.messages.create({ toPersonEmail: notifyEmail, text });
    }
    console.log('Suspect bot has lost connection');
  }
}

module.exports = (opts) => {

  options = opts;
  const { heartBeatSeconds, token, botEmail } = opts;
  if (!heartBeatSeconds) {
    console.error('No heartBeatSeconds defined');
    return () => {};
  }
  if (!token || !botEmail) {
    console.error('token or botEmail not defined');
    return () => {};
  }

  webex = Webex.init({
    credentials: {
      access_token: options.token,
    }
  });

  setInterval(check, pollIntervalSeconds * 1000);
  setInterval(sendHeartBeat, heartBeatSeconds * 1000);
  setTimeout(sendHeartBeat, 5000); // send initial beat
  console.log('Bot monitor is active, heart beat interval is ', heartBeatSeconds, 's');
  return onMsg;
};
