const Webex = require('webex');

class Bot {

  constructor(token, callback) {
    this.callback = callback;
    this.webex = Webex.init({
      credentials: {
        access_token: token,
      }
    });
    this.whoAmI()
      .then(() => this.register())
      .catch((e) => console.error('Not able to init bot', e));
  }

  createMessage(msg) {
    return this.webex.messages.create(msg);
  }

  onEvent(type, e) {
    if (this.callback) {
      this.callback(type, e);
    }
  }

  whoAmI() {
    return this.webex.people.get('me')
      .then(person => {
        this.me = person;
      });
  }

  reconnect() {
    const url = this.webex.internal.mercury.socket.url;
    console.log('reconnecting to', url);
    this.webex.internal.mercury._reconnect(url);
  }

  register() {
    this.webex.messages.listen()
      .then(() => {
        console.log('Bot is ready:', this.me.displayName);
        this.webex.messages.on('created', (e) => {
          const messageIsMine = e.data.personId === this.me.id
          if (messageIsMine) return;
          this.onEvent('message', e);
        });
      });

    this.webex.attachmentActions.listen()
      .then(() => {
        this.webex.attachmentActions.on('created', (event) => {
          this.onEvent('card', event);
        });
      });
  }
}


module.exports = Bot;
