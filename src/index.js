/**
 * A bot micro framework that is similar to Express's middleware structure and routing.
 * Instead of post, get etc we add listeners with regexs to the incoming message text
 *
 * For now, the only supported event types are new message and new card
 */


const monitor = require('./middleware/monitor');
const Bot = require('./bot');

const messageListeners = [];
let middlewares = [];
let cardListener;
let bot;

function message(pattern, callback) {
  messageListeners.push({ pattern, callback });
}

function card(callback) {
  cardListener = callback;
}

function webex() {
  return bot.webex;
}

function using(middleware) {
  if (typeof middleware !== 'function') {
    console.error('middle ware is not function');
    return;
  }
  middlewares.push(middleware);
}

function onEvent(type, event) {
  // console.log('event', event);
  const message = event.data;
  const messageText = message.text;

  const request = {
    text: messageText,
    message: event.data,
    type,
  };

  const response = {
    answer: (reply) => {
      return bot.createMessage({
        roomId: event.data.roomId,
        markdown: reply,
      });
    },
    answerCard: (card, fallback) => {
      return bot.createMessage({
        roomId: event.data.roomId,
        text: fallback,
        attachments: {
          contentType: 'application/vnd.microsoft.card.adaptive',
          content: card,
        },
      });
    },
    send: (msg) => {
      return bot.createMessage(msg);
    },
    sendCard: (msg, card, fallback) => {
      msg.attachments = {
        contentType: 'application/vnd.microsoft.card.adaptive',
        content: card,
      };
      msg.text = fallback;

      return bot.createMessage(msg);
    }
  }

  for (let m = 0; m < middlewares.length; m++) {
    const mw = middlewares[m];
    const stop = mw(request, response);
    if (stop) return; // middleware terminated the chain
  }

  if (type === 'message') {
    for (let i = 0; i < messageListeners.length; i++) {
      const listener = messageListeners[i];
      const text = messageText.toLowerCase();
      const { pattern, callback } = listener;

      // string matching
      if (typeof pattern === 'string' && text.includes(pattern)) {
        callback(request, response);
        return;
      }

      // regex matching
      else if (pattern.exec && messageText.match(pattern, 'i')) {
        callback(request, response);
        return;
      }
    }
  }
  else if (type === 'card') {
    if (cardListener) {
      cardListener(event.data, response);
    }
  }
}

function listen(token) {
  bot = new Bot(token, onEvent);
}

module.exports = {
  listen,
  message,
  card,
  webex,
  using,
  middleware: {
    monitor,
  },
};
